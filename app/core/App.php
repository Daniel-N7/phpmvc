<?php

class App
{
    protected $controller = 'Home';
    protected $method = 'index';
    protected $params = [];

    public function __construct()
    {
        // panggil fungsi parseURL.
        $url = $this->parseUrl();

        // controller
        if (isset($url)) {
            if (file_exists('../app/controllers/' . $url[0] . '.php')) {
                $this->controller = $url[0];
                unset($url[0]);
            }
        }
        
        // memanggil controller default
        require_once '../app/controllers/' . $this->controller . '.php';
        $this->controller = new $this->controller;

        // method
        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        // parameter
        if (!empty($url)) {
            $this->params = array_values($url);
        }

        // jalankan controller & method, serta kirimkan parameter jika ada
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    // function untuk 'mempercantik' url
    public function parseUrl()
    {
        if (isset($_GET['url'])) {
            // untuk menghapus '/' di bagian akhir url
            $url = rtrim($_GET['url'], '/');
            // untuk memfilter url agar bersih dari string-string aneh
            $url = filter_var($_GET['url'], FILTER_SANITIZE_URL);
            // untuk memisah nilai array antara '/'
            $url = explode('/', $url);
            return $url;
        }
    }
}
