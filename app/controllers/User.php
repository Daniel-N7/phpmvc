<?php

class User extends Controller
{
    public function index()
    {
        $data["judul"] = "User";
        $this->view("templates/header", $data);
        $this->view("templates/navbar");
        $this->view("user/index", $data);
        $this->view("templates/footer");
    }

    public function signup() {
        $data["user"] = $this->model("User_model")->getSignUp($_POST);
    }

    public function signin() {
        $row = $this->model("User_model")->getSignIn($_POST);
        // var_dump($row);
        // exit;
        if(count($row) > 0) {
            // echo "<script>Login berhasil</script>";
            // $this->view('home/index');
            $BASE_URL = BASE_URL;
            header("Location: $BASE_URL/home/index");
        } else {
            // echo "<script>Login gagal</script>";
            header("Location: login");
            // $error = true;
            exit;
        }
    }

    public function profile($nama = "Daniel", $pekerjaan = "Devs")
    {
        $data["judul"] = "User";
        $data["nama"] = $nama;
        $data["pekerjaan"] = $pekerjaan;
        $this->view("templates/header", $data);
        $this->view("templates/navbar");
        $this->view("user/profile", $data);
        $this->view("templates/footer");
    }

    public function login() {
        $data["judul"] = "Login";
        $this->view("templates/header", $data);
        $this->view("user/login");
    }

    public function register() {
        $data["judul"] = "Register";
        $this->view("templates/header", $data);
        $this->view("user/register");
    }
}
