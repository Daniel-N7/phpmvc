<div class="container card mt-4 w-50">
    <form action="<?= BASE_URL; ?>/user/signin" method="post">
        <h1>Login</h1>
        <!-- <?php if ($error) : ?>
            require_once ""
            <div class="alert alert-danger" role="alert">
                Username / password yg anda masukkan salah
            </div>
        <?php endif; ?> -->
        <div class="mb-3">
            <label for="username" class="form-label">Username</label>
            <input type="text" name="username" class="form-control" id="username" required autocomplete="off">
        </div>
        <div class="mb-3">
            <label for="pass" class="form-label">Password</label>
            <input type="password" name="pass" class="form-control" id="pass" required>
        </div>
        <!-- <div class="mb-3 form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Check me out</label>
        </div> -->
        <button type="submit" class="btn btn-primary mb-3">Login</button>
    </form>
</div>