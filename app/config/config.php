<?php
// Root URL
define('BASE_URL', 'http://localhost/phpmvc/public');
define('MD5_SALT', '*-abc');

// Database Credentials
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'phpmvc');